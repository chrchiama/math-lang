//
// Created by development on 24/03/20.
//

#ifndef MATH_LANG_REPORT_H
#define MATH_LANG_REPORT_H

#include <string>

using namespace std;

struct Report {
    static string error(int line, int column,const string& message);
};

#endif //MATH_LANG_REPORT_H
