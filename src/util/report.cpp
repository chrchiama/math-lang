//
// Created by development on 24/03/20.
//

#include "include/report.h"


string Report::error(int line, int column, const string& message) {
    string result = to_string(line + 1) + ":" + to_string(column + 1) + ": " + message;
    return result;
}

