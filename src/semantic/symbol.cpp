//
// Created by development on 24/03/20.
//

#include "include/symbol.h"

Semantic_Analisys::Symbol::Symbol() = default;
Semantic_Analisys::Symbol::Symbol(char *identifier, char *type, int line, int column) {
    this->identifier = identifier;
    this->type = type;
    this->line = line;
    this->column = column;
}

char *Semantic_Analisys::Symbol::get_identifier() { return this->identifier; }
char *Semantic_Analisys::Symbol::get_type() { return this->type; }
int Semantic_Analisys::Symbol::get_line() { return this->line; }
int Semantic_Analisys::Symbol::get_column() { return this->column; }