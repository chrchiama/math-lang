//
// Created by development on 24/03/20.
//

#ifndef MATH_LANG_SYMBOL_H
#define MATH_LANG_SYMBOL_H

namespace Semantic_Analisys {
    class Symbol;
}
class Semantic_Analisys::Symbol
{
public:
    Symbol();
    Symbol(char* identifier, char* type, int line, int column);
    char* get_identifier();
    char* get_type();
    int get_line();
    int get_column();
private:
    char* identifier{};
    char* type{};
    int line{};
    int column{};
};

#endif //MATH_LANG_SYMBOL_H
