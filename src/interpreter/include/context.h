//
// Created by development on 23/03/20.
//


#ifndef MATH_CONTEXT_H
#define MATH_CONTEXT_H


#include <iostream>
#include <map>

using namespace std;

namespace Interpreter {
    class Context;
}

class Interpreter::Context {

public:
    Context() = default;  // constructor
    Context(const Context &) = default;  // copy constructor
    Context(Context &&) = default;  // move constructor

private:
    map<string, string> classes;
    int environmen;
    int store;
    int self;

};

#endif //MATH_CONTEXT_H
