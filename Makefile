output: main.o
	g++ main.o -o  ./output

main: main.cpp
	g++ -c main.cpp

run:
	./output

clean:
	rm -rf *.o output